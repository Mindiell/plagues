Plagues is a python framework library for curses.

It aims to give people a better tool to work with curses, not being stopped by placing characters
on screen by lines or calculating if you are writing on another character somewhere.

plagues uses 4 concepts :

 * Application
 * Panel
 * Window
 * Templates

_Application_ is an object embedding the main application while managing the curses library.

As the application will display some "screens" where different information will be shown, those
"screens" are called _Panels_ in _Plagues_. So, each time a different information is needed on your
screen, you'll use a different panel.

In order to display informations, you'll have to put data on screen. For this, a _Panel_ is built on
a list of _Window_s. Each window is a part of the screen by having coordinates, width and height.
Finally, each _Window_ can use some _Template_.

