#encoding:utf-8
"""
Plagues for curses... ah ah ah ;o)

This library is here to help developers to create curses software.

license: AGPLv3+
author:  https://framagit.org/Mindiell
website: https://framagit.org/Mindiell/plagues
"""

import curses
from jinja2 import Environment, FileSystemLoader
import re

ATTRIBUTES = {
    "w": curses.A_BLINK,
    "b": curses.A_BOLD,
    "d": curses.A_DIM,
    "r": curses.A_REVERSE,
    "s": curses.A_STANDOUT,
    "u": curses.A_UNDERLINE,
}

COLORS = (
    curses.COLOR_BLACK,
    curses.COLOR_RED,
    curses.COLOR_BLUE,
    curses.COLOR_GREEN,
    curses.COLOR_MAGENTA,
    curses.COLOR_CYAN,
    curses.COLOR_YELLOW,
    curses.COLOR_WHITE,
)

class Part:
    def __init__(self, text, attribute, row, column):
        self.text = text
        self.attribute = attribute
        self.row = row
        self.column = column


class Window(object):
    """
    Each window is a pad into which a template is rendered.
    When refreshed on the screen, only a part of the pad is used.
    """
    def __init__(self, template, panel_dimensions, coords, left_corner, dimensions):
        """
        Set the template to use for rendering and all the numeric variables:
        - dimensions are pad dimensions nothing to do with real screen (see curses documentation)
        - coords are coordinates of top-left-corner of pad being rendered on screen
        - left_corner are coordinates of the top-left-corner where the pad is rendered on screen
        - right_corner are coordinates of the bottom-right-corner where the pad is rendered on
          screen
        """
        self.template = template
        (self.width, self.height) = panel_dimensions
        (self.coord_x, self.coord_y) = coords
        (self.left_corner_x, self.left_corner_y) = left_corner
        (self.right_corner_x, self.right_corner_y) = (
            self.left_corner_x+dimensions[0],
            self.left_corner_y+dimensions[1]
        )
        self.pad = curses.newpad(self.height, self.width)

    def render(self, environment, context):
        """
        Content of pad is refreshed based on the template and the context.
        """
        self.pad.clear()
        if self.template:
            result = environment.get_template(self.template).render(context)
            parts = self.split_in_parts(result)
            # Write each part of text with its own attribute
            for part in parts:
                self.pad.addnstr(part.row, part.column, part.text, self.width-part.column, part.attribute)

    def split_in_parts(self, content):
        results = re.findall(r"(<[^>]+>)|([^<]+)", content)
        row = 0
        column = 0
        attribute = 0
        color = 0
        colors = []
        parts = []
        for result in results:
            if result[0]!="":
                if result[0]=="<<>":
                    # False tag just to insert a simple <
                    parts.append(Part("<", attribute, row, column))
                    column += 1
                else:
                    # It's a tag
                    tag = result[0][1:-1]
                    end_tag = False
                    if tag.startswith("/"):
                        # It's an end tag
                        end_tag = True
                        tag = tag[1:]
                    if re.match("^c[0-9]{2}$", tag):
                        # It's a color tag
                        new_color = int(re.search("[0-9]{2}", tag).group(0))
                        if end_tag:
                            # Removing old color
                            attribute &= ~curses.color_pair(color)
                            if len(colors)>0:
                                color = colors.pop()
                            else:
                                color = 0
                            # Using new color
                            attribute |= curses.color_pair(color)
                        else:
                            # Removing old color
                            attribute &= ~curses.color_pair(color)
                            colors.append(color)
                            color = int(new_color/10)*8+(new_color%10)+1
                            # Using new color
                            attribute |= curses.color_pair(color)
                    elif tag in "wbdrsu":
                        # It's an attribute tag
                        if end_tag:
                            attribute &= ~ATTRIBUTES[tag]
                        else:
                            attribute |= ATTRIBUTES[tag]
                # Unknown tags are cut silently
            else:
                # Some text
                if "\n" in result[1]:
                    texts = result[1].split("\n")
                    for text in texts[:-1]:
                        if text!="":
                            parts.append(Part(text, attribute, row, column))
                        column = 0
                        row += 1
                    text = texts[-1]
                    if text!="":
                        parts.append(Part(text, attribute, row, column))
                        column += len(text)
                else:
                    text = result[1]
                    parts.append(Part(text, attribute, row, column))
                    column += len(text)
        return parts


class Panel(object):
    """
    A Panel is a list of actions and windows linked to a specific context:
    - actions are what is being done on keypress
    - windows are subscreens (template based) being printed on main screen
    - context fill variables of windows templates

    Application is passed to the class in order to use templates and screen objects.
    """
    def __init__(self, app):
        """
        By default, no action and no windows, thus no context.
        """
        self.app = app
        self.actions = {}
        self.windows = []
        self.context = {}

    def action(self, key):
        """
        This method is calling specific methods based on possible actions.
        Since the same method can be used for different actions, and some keys could be retrieved
        without being an action, the pressed key is passed to the action.
        Action methods should return True or False. False being an indicator to terminate the
        application.
        """
        for keys in [key for key in self.actions.keys() if key]:
            if key in keys:
                return self.actions[keys](key)
        if None in self.actions.keys():
            return self.actions[None](key)

    def draw(self):
        """
        This method will render each window using the context, then draw each of them on the main
        screen.
        """
        for window in self.windows:
            window.render(self.app.environment, self.context)
            window.pad.noutrefresh(
                window.coord_y, window.coord_x,
                window.left_corner_y, window.left_corner_x,
                window.right_corner_y, window.right_corner_x
            )
        curses.doupdate()


class Application(object):
    """
    This class is a Base class needed to use plagues.
    Use it by passing it some Panels. First panel will be used at starting panel.
    """
    def __init__(self, panels=None, template_folder=""):
        """
        Set the list of panels to use and initialize the templates environment.
        """
        self.screen = None
        self.screen_height, self.screen_width = (0, 0)
        if panels is not None:
            self.class_panels = panels
        else:
            self.class_panels = {"": Panel}
        self.panels = {}
        self.panel = None
        self.environment = Environment(
            loader=FileSystemLoader(template_folder),
            trim_blocks=True
        )

    def run(self):
        """
        In order to use curses properly, launch the application inside the curses wrapper.
        """
        curses.wrapper(self._run)

    def _run(self, screen=None):
        """
        Initialize curses screen, then instanciate panels. We are forced to do so because panels
        use screen information.
        Then, the main loop draws the active panel and waits for a key being entered.
        """
        self.screen = screen
        self.screen_height, self.screen_width = self.screen.getmaxyx()
        # Hiding cursor
        curses.curs_set(0)
        # Initializing colors
        color = 1
        for front in range(8):
            for back in range(8):
                if color==0:
                    color += 1
                    break
                if color>=curses.COLOR_PAIRS:
                    break
                curses.init_pair(color, COLORS[front], COLORS[back])
                color += 1
        # Instanciating panels
        for name, panel in self.class_panels.items():
            self.panels[name] = panel(self)
            if self.panel is None:
                self.set_panel(name)
        while True:
            self.panel.draw()
            if self.panel.action(self.screen.getkey()):
                break

    def set_panel(self, panel_name):
        """
        To take another panel in account, it's better to use its name and let the application manage
        it.
        """
        if panel_name in self.panels.keys():
            if self.screen:
                self.screen.clear()
                self.screen.refresh()
            self.panel = self.panels[panel_name]
